package com.example.rika20

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class MainActivity : AppCompatActivity() {
    val list = ArrayList<siswa>()
    val listnama = arrayOf(
        "MUHAMMAD RIFQIL KHANIF",
        "NABHAN AFLAHU SYAFIQ",
        "NATASYA AYU LESTANTI",
        "NOVI INDRIYNANI",
        "NUR HESTI MUGI RAHAYU",
        "RIKA RAHMA AULIA",
        "SALMA NADIA AGUSTINA",
        "SALMA VILINDIA PUTRI",
        "SILVI WIDYA MAULANI",
        "SITI KOTIJAH",
        "VIVI NUR AISYAH"
    )

    val listNis = arrayOf(
        "3015",
        "3017",
        "3019",
        "3021",
        "3023",
        "3025",
        "3028",
        "3029",
        "3031",
        "3033",
        "3035"
    )

    lateinit var makananView: RecyclerView
    lateinit var  makananAdapter: makananAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        makananView = findViewById(R.id.nama)
        makananView.layoutManager = LinearLayoutManager( this)

        for (i in listnama.indices){
            list.add(siswa(listnama[i], listNis[i]))
        }

        makananAdapter = makananAdapter(list)
        makananAdapter.notifyDataSetChanged()
        makananView.adapter = makananAdapter
    }
}
